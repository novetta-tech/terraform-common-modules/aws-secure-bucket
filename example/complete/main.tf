resource "aws_kms_key" "this" {
  description             = "This key is used to encrypt S3 buckets"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket" "log_bucket" {
  bucket = "my-tf-log-bucket"
  acl    = "log-delivery-write"
}

module "this" {
  source = "../src"

  enable = true

  prefix = "mdh"
  name   = "my-tf-test-bucket"
  region = "us-west-2"

  bucket_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY

  enable_versioning     = true
  create_encryption_key = false
  kms_key               = aws_kms_key.this.arn
  log_bucket            = aws_s3_bucket.log_bucket.id

  force_destroy = true

  tags = {
    App = "tester"
  }
}