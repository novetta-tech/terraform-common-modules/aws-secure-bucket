FROM alpine:3.11 as base

RUN apk update

RUN apk add --no-cache \
  make \
  curl \
  musl-dev \
  git \
  zip \
  unzip \
  shellcheck \
  go

ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV PATH /go/bin:$PATH

RUN mkdir -p ${GOPATH}/src ${GOPATH}/bin

ENV PATH="/opt/go/bin:/root/bin:/${PATH}"

# Install Terraform
RUN curl --insecure -L https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip --output terraform.zip && \
    unzip terraform.zip -d /usr/local/bin && \
    rm terraform.zip && \
    terraform --version && \
    echo "----------------- Terraform Successfully Installed ------------------"

# Install tflint
RUN curl --insecure -L https://github.com/terraform-linters/tflint/releases/download/v0.15.4/tflint_linux_amd64.zip --output terraform-lint.zip && \
    unzip terraform-lint.zip -d /usr/local/bin && \
    rm terraform-lint.zip && \
    tflint --version && \
    echo "---------- tflint Successfully Installed ----------"

# Install tfsec
RUN curl --insecure -L https://github.com/liamg/tfsec/releases/download/v0.19.0/tfsec-linux-amd64 --output /usr/local/bin/tfsec && \
    chmod +x /usr/local/bin/tfsec && \
    tfsec --version && \
    echo "---------- tfsec Successfully Installed ----------"

# Install terraform-docs
RUN curl --insecure -L https://github.com/segmentio/terraform-docs/releases/download/v0.9.1/terraform-docs-v0.9.1-linux-amd64 --output /usr/local/bin/terraform-docs && \
    chmod +x /usr/local/bin/terraform-docs && \
    terraform-docs --version && \
    echo "--------------- Terraform-docs Successfully Installed ---------------"


RUN mkdir /code
COPY . /code
WORKDIR /code

RUN addgroup -S developers && adduser -S dev -G developers
USER dev
