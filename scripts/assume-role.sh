#! /bin/bash

usage="Usage: $(basename "$0") accountId [serviceRole] [sessionSuffix] [aws-cli-opts]

where:
  accountId     - AWS account id
  serviceRole   - AWS service role to assume
  sessionSuffix - Unique session suffix
  aws-cli-opts  - Additional AWS CLI suffix
"

if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "help" ] || [ "$1" == "usage" ] ; then
  echo "$usage"
  exit -1
fi

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] ; then
  echo "$usage"
  exit -1
fi

shopt -s failglob
set -eu -o pipefail

unset   AWS_SESSION_TOKEN
export  AWS_DEFAULT_REGION="us-east-1"

temp_role=$(aws sts assume-role \
                    --role-arn "arn:aws:iam::${1}:role/${2:CICDServiceRole}" \
                    --role-session-name "${3}" \
                    ${@:4})

export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)

env | grep -i AWS_
