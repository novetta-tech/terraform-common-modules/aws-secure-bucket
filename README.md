# AWS Secure S3 Bucket
Terraform module which creates a standard secure S3 bucket.

## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.42 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| bucket\_policy | (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide. | `string` | `""` | no |
| create\_encryption\_key | (Optional) A boolean flag to create a specific KMS key for the S3 bucket. | `bool` | `true` | no |
| enable | A boolean flag to enable the creation of the S3 bucket resources. | `bool` | `true` | no |
| enable\_versioning | (Optional) A boolean flag to enable object versioning. | `bool` | `true` | no |
| environment\_name | An environment name to identify and separate S3 buckets. | `any` | n/a | yes |
| force\_destroy | (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable. | `bool` | `false` | no |
| kms\_key | (Optional) Required if create\_encryption\_key is false. ARN of KMS master key to encrypt bucket contents. | `any` | n/a | yes |
| kms\_policy | (Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide. | `string` | `""` | no |
| log\_bucket | S3 log bucket id to send access logs to. | `any` | n/a | yes |
| name | (Forces new resource) The name of the bucket. | `any` | n/a | yes |
| prefix | (Optional) Prefix to add to beginning of the bucket name. | `string` | `"nvt"` | no |
| region | (Optional, Default: us-east-1) The AWS region this bucket should reside in. | `string` | `"us-east-1"` | no |
| tags | (Optional) A mapping of tags to assign to the bucket. | `map` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| this\_kms\_key\_arn | The ARN of the KMS key used to encrypt the S3 bucket. |
| this\_s3\_bucket\_arn | The ARN of the bucket. Will be of format arn:aws:s3:::bucketname. |
| this\_s3\_bucket\_bucket\_domain\_name | The bucket domain name. Will be of format bucketname.s3.amazonaws.com. |
| this\_s3\_bucket\_bucket\_regional\_domain\_name | The bucket region-specific domain name. The bucket domain name including the region name, please refer here for format. Note: The AWS CloudFront allows specifying S3 region-specific endpoint when creating S3 origin, it will prevent redirect issues from CloudFront to S3 Origin URL. |
| this\_s3\_bucket\_hosted\_zone\_id | The Route 53 Hosted Zone ID for this bucket's region. |
| this\_s3\_bucket\_id | The name of the bucket. |
| this\_s3\_bucket\_region | The AWS region this bucket resides in. |

