provider "aws" {
  version = "~> 2.42"
  region  = var.region
}
