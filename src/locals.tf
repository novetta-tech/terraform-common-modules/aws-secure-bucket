locals {
  prefix = var.prefix == "" ? "${var.prefix}-" : var.prefix
  name   = "${local.prefix}${var.environment_name}-${var.name}-${var.region}"
}