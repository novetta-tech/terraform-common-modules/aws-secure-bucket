resource "aws_kms_key" "this" {
  count = var.enabled && var.create_encryption_key ? 1 : 0

  description             = "This key is used to encrypt S3 bucket: ${local.name}"
  deletion_window_in_days = 15
  enable_key_rotation     = true

  policy = var.kms_policy != "" ? var.kms_policy : null

  tags = merge(var.tags, {
    Name            = var.name
    ForResourceType = "S3Bucket"
    ForResourceName = local.name
  })
}

resource "aws_s3_bucket" "this" {
  count = var.enabled ? 1 : 0

  region = var.region
  bucket = local.name
  acl    = "private"

  policy = var.bucket_policy != "" ? var.bucket_policy : null

  versioning {
    enabled = var.enable_versioning
  }

  lifecycle_rule {
    id = "standard_noncurrent_lc"

    enabled = var.enable_versioning

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = var.create_encryption_key ? aws_kms_key.this[0].arn : var.kms_key
        sse_algorithm     = "aws:kms"
      }
    }
  }

  logging {
    target_bucket = var.log_bucket
    target_prefix = "${local.name}/"
  }

  force_destroy = var.force_destroy

  tags = merge(
    var.tags,
    {
      Name = local.name
    }
  )
}

resource "aws_s3_bucket_public_access_block" "this" {
  count = var.enabled ? 1 : 0

  bucket = aws_s3_bucket.this[0].id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}