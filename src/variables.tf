variable "enabled" {
  description = "A boolean flag to enable the creation of the S3 bucket resources."
  default     = true
}

variable "environment_name" {
  description = "An environment name to identify and separate S3 buckets."
}

variable "name" {
  description = "(Forces new resource) The name of the bucket."
}

variable "prefix" {
  description = "(Optional) Prefix to add to beginning of the bucket name. "
  default     = "nvt"
}

variable "log_bucket" {
  description = "S3 log bucket id to send access logs to."
}

variable "region" {
  description = "(Optional, Default: us-east-1) The AWS region this bucket should reside in."
  default     = "us-east-1"
}

variable "bucket_policy" {
  description = "(Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide."
  default     = ""
}

variable "kms_policy" {
  description = "(Optional) A valid bucket policy JSON document. Note that if the policy document is not specific enough (but still valid), Terraform may view the policy as constantly changing in a terraform plan. In this case, please make sure you use the verbose/specific version of the policy. For more information about building AWS IAM policy documents with Terraform, see the AWS IAM Policy Document Guide."
  default     = ""
}

variable "enable_versioning" {
  description = "(Optional) A boolean flag to enable object versioning."
  default     = true
}

variable "create_encryption_key" {
  description = "(Optional) A boolean flag to create a specific KMS key for the S3 bucket."
  default     = true
}

variable "kms_key" {
  description = "(Optional) Required if create_encryption_key is false. ARN of KMS master key to encrypt bucket contents."
  default     = null
}

variable "force_destroy" {
  description = "(Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  default     = false
}

variable "tags" {
  description = "(Optional) A mapping of tags to assign to the bucket."
  default     = {}
}